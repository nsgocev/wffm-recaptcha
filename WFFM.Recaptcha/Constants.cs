﻿namespace WFFM.Recaptcha
{
    public static class Constants
    {
        public const string GoogleAPIVerifyURL = "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}";

        public const string RecaptchaResponseFieldName = "g-recaptcha-response";
    }
}