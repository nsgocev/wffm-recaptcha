﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using WFFM.Recaptcha.Services;

namespace WFFM.Recaptcha.Forms.MVC.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter,
        AllowMultiple = false)]
    public sealed class RecaptchaAttribute : DataTypeAttribute
    {
        public RecaptchaAttribute()
            : base(DataType.Text)
        {
            ErrorMessage = "RECaptcha Is Invalid";
        }

        public override bool IsValid(object value)
        {

            if (value == null || HttpContext.Current == null ||
                string.IsNullOrEmpty(HttpContext.Current.Request[Constants.RecaptchaResponseFieldName]))
            {
                return false;
            }

            string privateKey = value.ToString();

            return !string.IsNullOrEmpty(privateKey) &&
                   RecaptchaService.ValidateRecaptchaResponse(
                       HttpContext.Current.Request[Constants.RecaptchaResponseFieldName], privateKey);

        }
    }
}