﻿using Sitecore.Data.Items;
using Sitecore.Forms.Mvc.Models;
using WFFM.Recaptcha.Forms.MVC.Attributes;


namespace WFFM.Recaptcha.Forms.MVC.Models
{
    public class RecaptchaField : FieldModel
    {
        public override bool IsRequired
        {
            get { return false; }
        }

        [Recaptcha]
        public override object Value
        {
            get { return RecaptchaPrivateKey; }
            set { }
        }


        public RecaptchaField(Item item)
            : base(item)
        {

        }

        public string RecaptchaPublicKey { get; set; }

        public string RecaptchaPrivateKey { get; set; }
    }
}