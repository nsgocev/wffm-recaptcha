﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Recaptcha.ascx.cs" Inherits="WFFM.Recaptcha.Web.UI.UserControl.Recaptcha" %>
<%@ Register TagPrefix="wfmcustom" Namespace="Sitecore.Form.Web.UI.Controls" Assembly="Sitecore.Forms.Custom" %>

<asp:Panel ID="CaptchaLimitTextPanel" CssClass="scfCaptchaBorder" runat="server">
     <span class="scfCaptchaLabel">&nbsp;</span>
    <asp:Panel ID="CaptchaStrongTextPanel" CssClass="scfCaptchaGeneralPanel" runat="server">
          <wfmcustom:Label ID="ReCaptchaTextTitle" runat="server" CssClass="scfCaptchaLabelText"
                AssociatedControlID="RecaptchaHiddenField" Text="" />
        <asp:TextBox ID="RecaptchaHiddenField" runat="server" Visible="false"/>
        <asp:HiddenField runat="server" ID="RecaptchaPublicKeyHiddenField" />
        <asp:HiddenField runat="server" ID="RecaptchaPrivateKeyHiddenField" />
        <div id="recaptcha" class="g-recaptcha" runat="server"></div>
    </asp:Panel>
</asp:Panel>


<script src="https://www.google.com/recaptcha/api.js" async defer>
</script>

