﻿using System;
using System.ComponentModel;
using System.Web;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.StringExtensions;

namespace WFFM.Recaptcha.Web.UI.UserControl
{
    public partial class Recaptcha : ValidateUserControl, IHasTitle
    {
        public override string ID
        {
            get
            {
                return RecaptchaHiddenField.ID;
            }
            set
            {
                RecaptchaHiddenField.ID = value;
                ReCaptchaTextTitle.AssociatedControlID = value;
                base.ID = value + "border";
            }
        }

        protected override System.Web.UI.Control InnerValidatorContainer
        {
            get { return CaptchaStrongTextPanel; }
        }

        protected override System.Web.UI.Control ValidatorContainer
        {
            get { return CaptchaLimitTextPanel; }
        }

        public override ControlResult Result
        {
            get
            {
                string gCaptchaResponse = HttpContext.Current.Request[Constants.RecaptchaResponseFieldName];
                return new ControlResult(ControlName, gCaptchaResponse,
                    Sitecore.Form.Core.Configuration.Constants.SecureToken.FormatWith(new object[]
                    {
                        gCaptchaResponse
                    }));
            }
        }

        public string Title
        {
            get
            {
                return ReCaptchaTextTitle.Text;
            }
            set
            {
                ReCaptchaTextTitle.Text = value;
            }
        }

        [DefaultValue("scfCaptcha")]
        [VisualFieldType(typeof(CssClassField))]
        [VisualProperty("CSS Class:", 400)]
        public new string CssClass
        {
            get
            {
                return Attributes["class"];
            }
            set
            {
                Attributes["class"] = value;
            }
        }

        [VisualFieldType(typeof(EditField))]
        [VisualProperty("ReCaptcha Public Key:" ,500)]
        public string RecaptchaPublicKey
        {
            get { return RecaptchaPublicKeyHiddenField.Value; }
            set { RecaptchaPublicKeyHiddenField.Value = value; }
        }

        [VisualFieldType(typeof(EditField))]
        [VisualProperty("ReCaptcha Private Key:", 600)]
        public string RecaptchaPrivateKey
        {
            get { return RecaptchaPrivateKeyHiddenField.Value; }
            set { RecaptchaPrivateKeyHiddenField.Value = value; }
        }

        protected override void OnLoad(EventArgs e)
        {
            recaptcha.Attributes["data-sitekey"] = RecaptchaPublicKey;
        }
    }
}