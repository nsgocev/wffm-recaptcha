﻿if (typeof ($scw) === "undefined") {
    window.$scw = jQuery.noConflict(true);
}


$scw.widget("wffm.ajaxForm", {
    options: {
        targetId: null
    },

    _create: function() {
        var form = this.element;
        var options = this.options;
        form.submit(function() {
            if ($scw(this).valid()) {
                $scw.ajax({
                    url: this.action,
                    type: this.method,
                    processData: false,
                    contentType: false,
                    data: new FormData(form.get(0)),
                    success: function(result) {


                        $scw('#' + options.targetId).html($scw(result).find("#" + options.targetId).get(0).outerHTML);

                        if ($scw('#recaptcha').length > 0) {
                            $scw('#' + options.targetId).append("<script src='https://www.google.com/recaptcha/api.js' async defer></script>");
                        }
                    },
                    error: function(xhr, status, exception) {
                        $scw('#' + options.targetId).html(xhr.responseText);


                    }
                });
            }
            return false;
        });
    }
});