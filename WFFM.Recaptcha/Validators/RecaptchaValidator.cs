﻿using System.Web;
using System.Web.UI.WebControls;
using Sitecore.Form.Core.Validators;
using WFFM.Recaptcha.Services;

namespace WFFM.Recaptcha.Validators
{
    public class RecaptchaValidator : FormCustomValidator
    {
        protected override bool EvaluateIsValid()
        {
            return OnServerValidate(string.Empty);
        }

        protected override bool OnServerValidate(string value)
        {
            if (HttpContext.Current == null)
            {
                return false;
            }

            string gResponse = HttpContext.Current.Request[Constants.RecaptchaResponseFieldName];

            if (string.IsNullOrWhiteSpace(gResponse))
            {
                return false;
            }

            HiddenField privateKeyField = FindControl("RecaptchaPrivateKeyHiddenField") as HiddenField;

            if (privateKeyField != null && !string.IsNullOrWhiteSpace(privateKeyField.Value))
            {
                return RecaptchaService.ValidateRecaptchaResponse(gResponse, privateKeyField.Value);
            }

            return false;
        }

        
    }
}