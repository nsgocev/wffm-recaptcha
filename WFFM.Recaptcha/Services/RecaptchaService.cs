﻿using System.Net;
using Newtonsoft.Json;
using WFFM.Recaptcha.Models;

namespace WFFM.Recaptcha.Services
{
    public class RecaptchaService
    {
        public static bool ValidateRecaptchaResponse(string gCaptchaResponse, string privateKey)
        {
            if (string.IsNullOrWhiteSpace(gCaptchaResponse))
            {
                return false;
            }
            
            var client = new WebClient();
            var reply =
                client.DownloadString(
                    string.Format(Constants.GoogleAPIVerifyURL, privateKey,
                        gCaptchaResponse));

            var captchaResponse = JsonConvert.DeserializeObject<RecaptchaResponse>(reply);


            return captchaResponse.Success;
        }
    }
}